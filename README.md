TWOFAP - Two Factor Authentication Program

![](https://i.imgur.com/M37Hotm.png)

# Description

A simple, cross-platform, Qt-based TOTP (RFC 6238) generator. I was not able to find a similar existing program online that was still maintained and easy to compile so I decided to make my own.

The program will generate time-based one-time passwords as used by many popular websites and services such as Google, Microsoft, Twitter, Discord etc.

This program does not phone home, it does not track you, do anything intentionally malicious, or use any network resources of any kind. It works completely offline.

The license for this program is Apache 2.0. It comes with no warranty of any kind. Please do not blame me if the software eats your system, feeds your dog, destroys your disk or makes your cornflakes soggy.

The development is an early work-in-progress but should be stable enough for daily use.

On Windows and *nix, keys are currently stored in plain text in a `twofap.ini` file in the current working directory (not necessarily the same directory as the binary itself). On OSX it is in `~/Library/Application Support/twofap`. It is up to the user to make sure this file does not fall into the wrong hands. Later I might add an option to encrypt the file or something, but right now it is not a priority for me... 2FA is troublesome enough, I don't need 3FA.

The actual handling of the TOTP keys and the code generation itself is done using the public domain "libcppotp" library from Ondřej Hošek at https://github.com/RavuAlHemio/cpptotp. Thanks very much for that.

# Compiling

You will need Qt5 and a C++11 compiler toolchain. There are no other external dependencies.

First, fetch the repo with:

`git clone https://bitbucket.org/bparker06/twofap` or use your favorite Git GUI and provide the same URL to clone.

To build from the command-line:

Just run `qmake && make`.

To build from an IDE:

If you're using Qt Creator or another IDE that supports Qt (like Visual Studio with the Qt add-in), just open the .pro file and click Build.