#include <QApplication>
#include <QSettings>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication app(argc, argv);
  app.setOrganizationName("twofap");
  app.setApplicationName("twofap");
  app.setApplicationVersion("1.0.1");

  QSettings::setDefaultFormat(QSettings::IniFormat);

  MainWindow w;
  w.setWindowTitle(app.applicationName() + " " + app.applicationVersion());
  w.show();

  return app.exec();
}
