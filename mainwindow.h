#include <QMainWindow>
#include <QSettings>

class QTimer;
class QLabel;

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();
signals:
  void addKeyClicked();
private slots:
  void onAddKeyClicked();
  void rebuildLayout();
private:
  QWidget *m_centralWidget;
  QSettings m_settings;
  QLabel *m_statusLabel;
  QTimer *m_timer;

  void saveNewKey(QString name, QString key);
  QString generateCode(QString key);
};
